﻿using System.Web.Mvc;

namespace RoboSpeech.Controllers
{
    public class HomeController : Controller
    {
        // Get /home/index
        public ActionResult Index()
        {
            return View();
        }

        // Get /Home/About
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}