﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RoboSpeech.Startup))]
namespace RoboSpeech
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
